# Node + React + Express + MongoDB template

This is ready to use MERN template I made with a simple task list app working on it and using references from online tutorials like this: 
- https://medium.com/free-code-camp/create-a-fullstack-react-express-mongodb-app-using-docker-c3e3e21c4074

## Ports
- API: 9000
- Front End: 3000
- Mongo-express(graphic DB): 8081

## Start

Run ```./start.sh``` in your command line to exec the start script that will run docker-copose and react

## Issues

The client service at the ```docker-compose.yml``` document is commented because I couldn't create a React container, it just doesn't find react-scripts. React-scripts is only working in local if you run ```sudo npm install --save react-scripts``` in the React app folder.