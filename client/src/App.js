import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = { apiResponse: "" };
	}

	Header(props) {
		return <h1>Task list</h1>;
	  }

	callAPI() {
		fetch("http://localhost:9000/api")
			.then((res) => res.text())
			.then((res) => this.setState({ apiResponse: res }))
			.then((res) => console.log(this.state.apiResponse))
			.catch((err) => err);
	}

	componentDidMount() {
		this.callAPI();
	}

	render() {
		return (
			<div className="App">
				<this.Header></this.Header>
				<p className="App-intro">{this.state.apiResponse}</p>
			</div>
		);
	}
}

export default App;
